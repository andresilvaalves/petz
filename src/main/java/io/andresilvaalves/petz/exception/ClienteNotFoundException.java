package io.andresilvaalves.petz.exception;

public class ClienteNotFoundException extends RuntimeException {

    public ClienteNotFoundException(String s){
        super(s);
    }
}
