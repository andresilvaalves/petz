package io.andresilvaalves.petz.exception;

public class PetNotFoundException extends RuntimeException {

    public PetNotFoundException(String s){
        super(s);
    }
}
