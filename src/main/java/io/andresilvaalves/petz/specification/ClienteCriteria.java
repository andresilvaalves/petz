package io.andresilvaalves.petz.specification;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ClienteCriteria {

    private List<String> cpf;

    private Boolean ativo;

}
