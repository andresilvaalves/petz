package io.andresilvaalves.petz.specification;

import io.andresilvaalves.petz.model.Cliente;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ClienteSpecification implements Specification<Cliente> {

    private final ClienteCriteria clienteCriteria;

    public ClienteSpecification(ClienteCriteria clienteCriteria){
        this.clienteCriteria = clienteCriteria;
    }

    @Override
    public Predicate toPredicate(Root<Cliente> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if(clienteCriteria.getCpf() != null && !clienteCriteria.getCpf().isEmpty()){
            predicates.add(criteriaBuilder.in(root.get("id")).value(clienteCriteria.getCpf()));
        }

        predicates.add(criteriaBuilder.equal(root.get("ativo"), clienteCriteria.getAtivo()));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
