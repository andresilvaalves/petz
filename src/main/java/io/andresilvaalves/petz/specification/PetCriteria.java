package io.andresilvaalves.petz.specification;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PetCriteria {

    private String nome;

    private Boolean ativo;

}
