package io.andresilvaalves.petz.specification;

import io.andresilvaalves.petz.model.Pet;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class PetSpecification implements Specification<Pet> {

    private final PetCriteria petCriteria;

    public PetSpecification(PetCriteria petCriteria){
        this.petCriteria = petCriteria;
    }

    @Override
    public Predicate toPredicate(Root<Pet> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(criteriaBuilder.equal(root.get("ativo"), petCriteria.getAtivo()));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
