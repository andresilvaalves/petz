package io.andresilvaalves.petz.controller;

import io.andresilvaalves.petz.dto.ClienteDTO;
import io.andresilvaalves.petz.service.ClienteService;
import io.andresilvaalves.petz.specification.ClienteCriteria;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping
    public Page<ClienteDTO> findByFields(@RequestParam(value = "cpf", required = false) List<String> cpf,
                                         @RequestParam(value = "ativo", defaultValue = "true") Boolean ativo,
                                         @RequestParam(name = "page", defaultValue = "0") Integer page,
                                         @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        ClienteCriteria clienteCriteria = ClienteCriteria.builder()
                                                         .cpf(cpf)
                                                         .ativo(ativo)
                                                         .build();
        return clienteService.findByFields(clienteCriteria, page, pageSize);
    }

    @GetMapping("{uuid}")
    public ClienteDTO findById(@PathVariable("uuid") String uuid) {
        return clienteService.findById(UUID.fromString(uuid));
    }

    @PostMapping
    public ClienteDTO save(@NonNull @RequestBody ClienteDTO dto) {
        return clienteService.save(dto);
    }

    @PutMapping
    public ClienteDTO update(@NonNull @RequestBody ClienteDTO dto) {
        return clienteService.update(dto);
    }

    @DeleteMapping("{uuid}")
    public void delete(@PathVariable("uuid") String uuid) {
        clienteService.delete(UUID.fromString(uuid));
    }

}
