package io.andresilvaalves.petz.controller;

import io.andresilvaalves.petz.dto.PetDTO;
import io.andresilvaalves.petz.service.PetService;
import io.andresilvaalves.petz.specification.PetCriteria;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/pet")
public class PetController {

    @Autowired
    private PetService petService;

    @GetMapping
    public Page<PetDTO> findByFields(@RequestParam(value = "ativo", defaultValue = "true") Boolean ativo,
                                     @RequestParam(name = "page", defaultValue = "0") Integer page,
                                     @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        PetCriteria petCriteria = PetCriteria.builder()
                                             .ativo(ativo)
                                             .build();
        return petService.findByFields(petCriteria, page, pageSize);
    }

    @GetMapping("{uuid}")
    public PetDTO findById(@PathVariable("uuid") String uuid) {
        return petService.findById(UUID.fromString(uuid));
    }

    @PostMapping
    public PetDTO save(@NonNull @RequestBody PetDTO dto) {
        return petService.save(dto);
    }

    @PutMapping
    public PetDTO update(@NonNull @RequestBody PetDTO dto) {
        return petService.update(dto);
    }

    @DeleteMapping("{uuid}")
    public void delete(@PathVariable("uuid") String uuid) {
        petService.delete(UUID.fromString(uuid));
    }

}
