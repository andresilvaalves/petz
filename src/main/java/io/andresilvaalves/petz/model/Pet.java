package io.andresilvaalves.petz.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Pet extends PetzEntity {

    private String nome;

    private String especie;

    private String raca;

    @ManyToOne
    @JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "FK_cliente_pet"), nullable = false)
    @NotNull(message = "Cliente é Obrigatório")
    private Cliente cliente;

}
