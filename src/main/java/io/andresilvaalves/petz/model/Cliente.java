package io.andresilvaalves.petz.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@EqualsAndHashCode(callSuper = true, of = {"cpf"})
@Data
@Entity
public class Cliente extends PetzEntity {

    private String nome;

    @Email(message = "email inválido")
    private String email;

    @NotNull(message = "CPF obrigatório")
    @Column(name = "cpf", nullable = false, unique = true)
    @CPF(message = "CPF inválido")
    private String cpf;

    @OneToMany(mappedBy = "cliente", fetch = FetchType.EAGER)
    private List<Pet> pets;
}
