package io.andresilvaalves.petz.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class ClienteDTO {

    private UUID uuid;

    private String nome;

    private String email;

    private String cpf;

    private List<PetDTO> pets;

}
