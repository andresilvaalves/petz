package io.andresilvaalves.petz.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class PetDTO {

    private UUID uuid;

    private String nome;

    private String especie;

    private String raca;

    private ClienteDTO cliente;

}
