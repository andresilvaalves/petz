package io.andresilvaalves.petz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
public class PetzApplication {

    @Value("${app.message}")
    private String appMessage;

    public static void main(String[] args) {
        SpringApplication.run(PetzApplication.class, args);
    }

    @Bean
    public CommandLineRunner run() {
        return args -> {
            log.info(appMessage);

        };
    }

}
