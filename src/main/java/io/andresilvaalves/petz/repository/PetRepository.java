package io.andresilvaalves.petz.repository;

import io.andresilvaalves.petz.model.Pet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface PetRepository extends PagingAndSortingRepository<Pet, UUID>, JpaSpecificationExecutor<Pet> {

    Page<Pet> findAllByAtivoIsTrue(Pageable pageable);

}
