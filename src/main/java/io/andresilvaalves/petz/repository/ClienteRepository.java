package io.andresilvaalves.petz.repository;

import io.andresilvaalves.petz.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente, UUID>, JpaSpecificationExecutor<Cliente> {

    Page<Cliente> findAllByAtivoIsTrue(Pageable pageable);

}
