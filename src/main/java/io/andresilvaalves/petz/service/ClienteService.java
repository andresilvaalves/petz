package io.andresilvaalves.petz.service;

import io.andresilvaalves.petz.builder.ClienteBuilder;
import io.andresilvaalves.petz.dto.ClienteDTO;
import io.andresilvaalves.petz.exception.ClienteNotFoundException;
import io.andresilvaalves.petz.model.Cliente;
import io.andresilvaalves.petz.repository.ClienteRepository;
import io.andresilvaalves.petz.specification.ClienteCriteria;
import io.andresilvaalves.petz.specification.ClienteSpecification;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Page<ClienteDTO> findAll(Integer page, Integer pageSize) {
        Page<Cliente> clientes = clienteRepository.findAllByAtivoIsTrue(PageRequest.of(page, pageSize));
        return clientes.map(ClienteBuilder::builderToDTO);
    }

    public ClienteDTO findById(@NonNull UUID uuid) {
        Cliente cliente = clienteRepository.findById(uuid).orElseThrow(() -> new ClienteNotFoundException(uuid.toString()));
        return ClienteBuilder.builderToDTO(cliente);
    }

    public ClienteDTO save(@NonNull ClienteDTO dto) {
        Cliente cliente = new Cliente();
        ClienteBuilder.builderToEntity(cliente, dto);

        Cliente saved = clienteRepository.save(cliente);
        return ClienteBuilder.builderToDTO(saved);
    }

    public ClienteDTO update(@NonNull ClienteDTO dto) {
        Cliente cliente = clienteRepository.findById(dto.getUuid()).orElseThrow(() -> new ClienteNotFoundException(dto.getUuid().toString()));

        ClienteBuilder.builderToEntity(cliente, dto);
        Cliente saved = clienteRepository.save(cliente);
        return ClienteBuilder.builderToDTO(saved);
    }

    public void delete(@NonNull UUID uuid) {
        Cliente cliente = clienteRepository.findById(uuid).orElseThrow(() -> new ClienteNotFoundException(uuid.toString()));
        cliente.setAtivo(Boolean.FALSE);
        cliente.getPets().forEach(p -> p.setAtivo(Boolean.FALSE));
        clienteRepository.save(cliente);
    }

    public Page<ClienteDTO> findByFields(@NonNull ClienteCriteria clienteCriteria, Integer page, Integer pageSize) {
        Page<Cliente> all = clienteRepository.findAll(new ClienteSpecification(clienteCriteria), PageRequest.of(page, pageSize));
        return all.map(ClienteBuilder::builderToDTO);
    }

}
