package io.andresilvaalves.petz.service;

import io.andresilvaalves.petz.builder.PetBuilder;
import io.andresilvaalves.petz.dto.PetDTO;
import io.andresilvaalves.petz.exception.ClienteNotFoundException;
import io.andresilvaalves.petz.exception.PetNotFoundException;
import io.andresilvaalves.petz.model.Cliente;
import io.andresilvaalves.petz.model.Pet;
import io.andresilvaalves.petz.repository.ClienteRepository;
import io.andresilvaalves.petz.repository.PetRepository;
import io.andresilvaalves.petz.specification.PetCriteria;
import io.andresilvaalves.petz.specification.PetSpecification;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PetService {

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public Page<PetDTO> findAll(Integer page, Integer pageSize) {
        Page<Pet> pets = petRepository.findAllByAtivoIsTrue(PageRequest.of(page, pageSize));
        return pets.map(PetBuilder::builderToDTO);
    }

    public PetDTO findById(@NonNull UUID uuid) {
        Pet pet = petRepository.findById(uuid).orElseThrow(() -> new PetNotFoundException(uuid.toString()));
        return PetBuilder.builderToDTO(pet);
    }

    public PetDTO save(@NonNull PetDTO dto) {
        Pet pet = new Pet();
        PetBuilder.builderToEntity(pet, dto);

        buscarCliente(dto, pet);

        Pet saved = petRepository.save(pet);
        return PetBuilder.builderToDTO(saved);
    }

    public PetDTO update(@NonNull PetDTO dto) {
        Pet pet = petRepository.findById(dto.getUuid()).orElseThrow(() -> new PetNotFoundException(dto.getUuid().toString()));

        PetBuilder.builderToEntity(pet, dto);
        buscarCliente(dto, pet);

        Pet saved = petRepository.save(pet);
        return PetBuilder.builderToDTO(saved);
    }

    public void delete(@NonNull UUID uuid) {
        Pet pet = petRepository.findById(uuid).orElseThrow(() -> new PetNotFoundException(uuid.toString()));
        pet.setAtivo(Boolean.FALSE);
        petRepository.save(pet);
    }

    public Page<PetDTO> findByFields(@NonNull PetCriteria petCriteria, Integer page, Integer pageSize) {
        Page<Pet> all = petRepository.findAll(new PetSpecification(petCriteria), PageRequest.of(page, pageSize));
        return all.map(PetBuilder::builderToDTO);
    }

    private void buscarCliente(PetDTO dto, Pet pet) {
        if (dto.getCliente() != null && dto.getCliente().getUuid() != null) {
            final UUID uuidCliente = dto.getCliente().getUuid();
            Cliente cliente = clienteRepository.findById(uuidCliente).orElseThrow(() -> new ClienteNotFoundException(uuidCliente.toString()));
            pet.setCliente(cliente);
        }
    }
}
