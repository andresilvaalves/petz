package io.andresilvaalves.petz.builder;

import io.andresilvaalves.petz.dto.PetDTO;
import io.andresilvaalves.petz.model.Pet;
import lombok.NonNull;

public class PetBuilder {

    public static PetDTO builderToDTO(@NonNull Pet pet) {
        return PetDTO.builder()
                     .uuid(pet.getUuid())
                     .nome(pet.getNome())
                     .raca(pet.getRaca())
                     .especie(pet.getEspecie())
                     .cliente(ClienteBuilder.builderToDTOSimplificado(pet.getCliente()))
                     .build();
    }

    public static PetDTO builderToDTOSimplificado(@NonNull Pet pet) {
        return PetDTO.builder()
                     .uuid(pet.getUuid())
                     .nome(pet.getNome())
                     .raca(pet.getRaca())
                     .especie(pet.getEspecie())
                     .build();
    }

    public static void builderToEntity(@NonNull Pet pet, @NonNull PetDTO dto) {
        pet.setAtivo(Boolean.TRUE);
        pet.setNome(dto.getNome());
        pet.setEspecie(dto.getEspecie());
        pet.setRaca(dto.getRaca());
    }
}
