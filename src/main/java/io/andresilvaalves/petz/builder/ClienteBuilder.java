package io.andresilvaalves.petz.builder;

import io.andresilvaalves.petz.dto.ClienteDTO;
import io.andresilvaalves.petz.dto.PetDTO;
import io.andresilvaalves.petz.model.Cliente;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class ClienteBuilder {

    public static ClienteDTO builderToDTO(@NonNull Cliente cliente){

        List<PetDTO> petsDTO = null;
        if (Optional.ofNullable(cliente.getPets()).isPresent()){
            petsDTO = cliente.getPets().stream().map(PetBuilder::builderToDTOSimplificado).collect(Collectors.toList());
        }

        return ClienteDTO.builder()
                         .uuid(cliente.getUuid())
                         .nome(cliente.getNome())
                         .cpf(cliente.getCpf())
                         .pets(petsDTO)
                         .email(cliente.getEmail())
                         .build();
    }

    public static ClienteDTO builderToDTOSimplificado(@NonNull Cliente cliente){
        return ClienteDTO.builder()
                         .uuid(cliente.getUuid())
                         .nome(cliente.getNome())
                         .cpf(cliente.getCpf())
                         .email(cliente.getEmail())
                         .build();
    }

    public static void builderToEntity(@NonNull Cliente cliente, @NonNull ClienteDTO dto) {
        cliente.setAtivo(Boolean.TRUE);
        cliente.setCpf(dto.getCpf());
        cliente.setEmail(dto.getEmail());
        cliente.setNome(dto.getNome());
    }
}
